# Installation

## Pre-built binary

You can download pre-built binary from [GitHub Release](https://github.com/Young-TW/yush/releases).

## Build from source

To build from source, please ensure CMake installed;
then, run the following commands in the directory after cloning this repository.

```sh
cmake . -B build
cmake --build build
```
