# Commands

`yush` provides it's builtin commanda and normal commands(executable file in $PATH).  

## Builtin commands

`yush` have some builtin commands like:  

1. alias
2. cat
3. cd
4. clear
5. cp
6. date
7. echo
8. function
9. help
10. ls
11. mkdir
12. mv
13. pwd
14. rm
15. set
16. touch
17. whoami
18. yush

## Normal commands

`yush` can read environment variables too.  
So it can run commands installed in system.  
like `neofetch`, `git`, `tmux` etc.  